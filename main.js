
class Account {
    constructor (balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}

class Person {
    constructor (firstName, lastName, accountList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountList = accountList;
    }

    _calculateBalance () {
        var sum = 0;

        for (let account of this.accountList) {
            sum += account.balance;
        }

        return sum;
    }

    addAccount (balance, currency, number) {
        this.accountList.push(new Account(balance, currency, number))
    }

    sayHello () {
        const calculateBalance = this._calculateBalance();

        return `My name is ${this.firstName} ${this.lastName} ${this.accountList.length} ${calculateBalance}`;
    }

    filterPositiveAccounts () {
        return this.accountList.filter((account) => {
            return account.balance > 0;
        });
    }

    findAccount (accountNumber) {
        return this.accountList.find((account) => {
            return account.number === accountNumber;
        })
    }

    withdraw (accountNumber, amount) {
        return new Promise((resolve, reject) => {
            const foundAccount = this.findAccount(accountNumber);

            if (foundAccount !== undefined && foundAccount.balance > amount) {
                setTimeout(() => {
                    foundAccount.balance -= amount;
                    resolve(`Account number: ${foundAccount.number}, updated balance: ${foundAccount.balance}, withdrawn money: ${amount}`);
                }, 3000);
            } else {
                reject("Cannot find account!");
            }
        });
    }
}
const Szymon = new Person("Szymon", "Kowalski", [new Account(234, "PLN", 3)]);
Szymon.addAccount(46, 'pln', 2);
console.log(Szymon.findAccount(2));
Szymon.withdraw(2, 100)
    .then((successMessage) => {
            console.log(successMessage);
        }) 
    .catch((failedMessage) => {
            console.log(failedMessage);
        });