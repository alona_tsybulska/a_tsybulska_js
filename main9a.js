(()=>{
    const Alona = new Person("Alona", "Tsybulska", [new Account(23445, "PLN", 3), new Account(46, "PLN", 2)]);

    console.log("Alona: ", Alona);

    let cardTitle = document.querySelector("h4.card-title");
    let cardText = document.querySelector("p.card-text");
    let fieldNumber = document.getElementById("number");
    let fieldAmount = document.getElementById("amount");
    let submitButton = document.querySelector("button.btn.btn-primary");

    // Title
    cardTitle.innerHTML = `${Alona.firstName} ${Alona.lastName}`;

    let accountList = document.createElement("ul");
    // Accounts
    Alona.accountList
    .map((account) => {
        let li = document.createElement("li");
        li.innerHTML = `Number: ${account.number}, Balance: ${account.balance}, Currency: ${account.currency}.`;
        li.id = `account-${account.number}`;

        return li;
    })
    .forEach(element => {
        accountList.appendChild(element)
    });;

    cardText.appendChild(accountList);

    // Submit button
    submitButton.addEventListener("click", (event) => {
        Alona.withdraw(fieldNumber.value, fieldAmount.value)
        .then(() => {
            Alona.findAccount(fieldNumber.value)
            .then((account) =>{
                document.querySelector(`li#account-${fieldNumber.value}`).innerHTML = `Number: ${account.number}, Balance: ${account.balance}, Currency: ${account.currency}.`;
            })
        })
        .catch((failedMessage) => {
            console.log(failedMessage);
        });
    });
})();    
